<?php

require_once "animal.php";
require_once "ape.php";
require_once "frog.php";

$sheep = new Animal("Shaun", 2, "false");

echo $sheep->name; // "shaun"
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
echo $sheep->cold_blooded; // false
echo "<br><br>";

$Sungokong = new Ape ("Kera Sakti", 2, "false");
echo $Sungokong->name; 
echo "<br>";
echo $Sungokong->legs; 
echo "<br>";
echo $Sungokong->cold_blooded; 
echo "<br>";
echo $Sungokong->get_yell ();
echo "<br><br>";

$kodok = new Frog ("Buduk", 4, "false");
echo $kodok->name; 
echo "<br>";
echo $kodok->legs; 
echo "<br>";
echo $kodok->cold_blooded;
echo "<br>";
echo $kodok->get_jump ();
echo "<br>";

?>